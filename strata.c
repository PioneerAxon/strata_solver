/*
 * Copyright (C) 2014 Arth Patel (PioneerAxon) <arth.svnit@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version. See http://www.gnu.org/copyleft/gpl.html for the full text of the
 * license.
 */

#include <stdlib.h>
#include <stdio.h>
#include <assert.h>
#include "strata_board.h"

Board*
get_board ()
{
	Board* board;
	int n, c;
	int l1, l2;
	printf ("Size of board : ");
	scanf ("%d", &n);
	printf ("Number of colors : ");
	scanf ("%d", &c);
	assert (c < n * n);
	assert (n > 0);
	assert (c > 0);
	board = board_new (n, c);
	printf ("Enter board colors :\n");
	for (l1 = 0; l1 < n; l1++)
	{
		for (l2 = 0; l2 < n; l2++)
		{
			scanf ("%d", &c);
			if (c > board_colors (board) || c < 0)
				c = 0;
			board_insert_entry (board, l1, l2, c);
		}
	}
	return board;
}

int
find_unique (Board* board, int* c)
{
	int l1, l2;
	int color = 0, count = 0, valid;
	for (l1 = 0; l1 < board_size (board) * 2; l1++)
	{
		if (board_solution_found (board, l1))
			continue;
		count = 0;
		color = *c;
		valid = 1;
		for (l2 = 0; l2 < board_size (board) * 2; l2++)
		{
			if (board_flag (board, l1, l2) != 0)
			{
				count++;
				if (color != board_flag (board, l1, l2))
				{
					valid = 0;
					break;
				}
			}
		}
		if (valid && count > 0)
		{
			return l1;
		}
	}
	for (l1 = 0; l1 < board_size (board) * 2; l1++)
	{
		if (board_solution_found (board, l1))
			continue;
		count = 0;
		color = 0;
		valid = 1;
		for (l2 = 0; l2 < board_size (board) * 2; l2++)
		{
			if (board_flag (board, l1, l2) != 0)
			{
				count++;
				if (count == 1)
					color = board_flag (board, l1, l2);
				else if (color != board_flag (board, l1, l2))
				{
					valid = 0;
					break;
				}
			}
		}
		if (valid && count > 0)
		{
			*c = color;
			return l1;
		}
	}
	*c = 0;
	return -1;
}

void
clear_entry (Board* board, int index)
{
	int l;
	int r, c;
	for (l = 0; l < board_size (board) * 2; l++)
	{
		if (board_flag (board, index, l) != 0)
		{
			if (index < board_size (board))
			{
				r = index;
				c = l - board_size (board);
			}
			else
			{
				c = index - board_size (board);
				r = l;
			}
			board_clear_entry (board, r, c);
		}
	}
}

void
print_solution (Board* board, int index, int color)
{
	assert (board);
	assert (index >= 0 && index < board_size (board) * 2);
	assert (color >= 0 && color <= board_colors (board));
	if (index < board_size (board))
		printf ("R");
	else
		printf ("C");
	printf ("%-2d", (index % board_size (board)) + 1);
	printf ("  =>  %d\n", color);
}

void
print_dont_cares (Board* board)
{
	assert (board);
	int l;
	for (l = 0; l < 2 * board_size (board); l++)
	{
		if (board_solution_found (board, l) == 0)
		{
			print_solution (board, l, 0);
		}
	}
}

void
solve (Board* board, int prev_color)
{
	int uniq, color = prev_color;
	uniq = find_unique (board, &color);
	if (uniq == -1)
	{
		print_dont_cares (board);
		return;
	}
	board_set_found (board, uniq);
	clear_entry (board, uniq);
	solve (board, color);
	print_solution (board, uniq, color);
}

int main ()
{
	Board* board;
	board = get_board ();
	solve (board, 1);
	return EXIT_SUCCESS;
}
