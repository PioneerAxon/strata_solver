#include "strata_board.h"

struct _board
{
	int n;
	int *internal_array;
	int **flags;
	int colors;
	int *solution_found;
};

Board*
board_new (int n, int colors)
{
	Board* new;
	int l;
	new = (Board*) malloc (sizeof (Board));
	assert (new);
	new->n = n;
	new->internal_array = (int*) malloc (sizeof (int) * (4 * n * n));
	assert (new->internal_array);
	for (l = 0; l < 4 * n * n; l++)
		new->internal_array [l] = 0;
	new->flags = (int**) malloc (sizeof (int*) * (2 * n));
	assert (new->flags);
	new->solution_found = (int*) malloc (sizeof (int) * (2 * n));
	assert (new->solution_found);
	for (l = 0; l < 2 * n; l++)
	{
		new->flags [l] = new->internal_array + (2 * n * l);
		new->solution_found [l] = 0;
	}
	new->colors = colors;
	return new;
}

void
board_destroy (Board* board)
{
	if (!board)
		return;
	if (board->flags)
		free (board->flags);
	if (board->internal_array)
		free (board->internal_array);
	if (board->solution_found)
		free (board->solution_found);
	free (board);
}

void
board_insert_entry (Board* board, int row, int col, int color)
{
	assert (board);
	assert (row < board->n && row >= 0);
	assert (col < board->n && col >= 0);
	assert (color <= board->colors && color >= 0);
	board->flags [row] [board->n + col] = color;
	board->flags [board->n + col] [row] = color;
}

void
board_clear_entry (Board* board, int row, int col)
{
	board_insert_entry (board, row, col, 0);
}

void
board_set_found (Board* board, int index)
{
	assert (board);
	assert (index >= 0 && index < board->n * 2);
	assert (board->solution_found [index] != 1);
	board->solution_found [index] = 1;
}

void
board_reset_found (Board* board, int index)
{
	assert (board);
	assert (index >= 0 && index < board->n * 2);
	assert (board->solution_found [index] == 1);
	board->solution_found [index] = 0;
}

int
board_colors (Board* board)
{
	assert (board);
	return board->colors;
}

int
board_size (Board* board)
{
	assert (board);
	return board->n;
}

int
board_solution_found (Board* board, int index)
{
	assert (board);
	assert (board->solution_found);
	assert (index >= 0 && index < board->n * 2);
	return board->solution_found [index];
}

int
board_flag (Board* board, int x, int y)
{
	assert (board);
	assert (board->flags);
	assert (x >= 0 && y >= 0 && x < board->n * 2 && y < board->n * 2);
	assert (board->flags [x]);
	return board->flags [x][y];
}
