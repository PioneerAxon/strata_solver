#ifndef __STRATA_BOARD_H__
#define __STRATA_BOARD_H__

#include <stdlib.h>
#include <assert.h>

typedef struct _board Board;

Board* board_new (int size, int colors);

void board_destroy (Board* board);

void board_insert_entry (Board* board, int row, int col, int color);

void board_clear_entry (Board* board, int row, int col);

void board_set_found (Board* board, int index);

void board_reset_found (Board* board, int index);

int board_colors (Board* board);

int board_size (Board* board);

int board_solution_found (Board* board, int index);

int board_flag (Board* board, int index_x, int index_y);

#endif
