CC=gcc
CFLAGS=-Wall -O3
LDFLAGS=

strata-solver:strata_board.o strata.c
	$(CC) $(CFLAGS) $? -o $@

clean:
	rm -rf strata-solver *.o
